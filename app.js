
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var webshot = require('webshot');


var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/add',function(req,res){
    res.render("add",{params:req.query});

})

app.get('/share',function(req,res){
    var fs      = require('fs');


    webshot('www.tripadvisor.com',{quality:100}, function(err, renderStream) {
        var start  = (new Date()).getTime();
        var file_name = path.join(__dirname,"/public/images", start+".png");


        var file = fs.createWriteStream(file_name, {encoding: 'binary'});

        renderStream.on('data', function(data) {
            console.log("progressing...")
            file.write(data.toString('binary'), 'binary');
        });

        renderStream.on('end',function(){
            var end  = (new Date()).getTime();
            console.log("some error when getting binary.");
            file.end();
            //res.render("share",{params : {},img : start+".png"});
        })
    });


//    console.log(req.query);
//    req.query.h = "http://www.google.com"
//    console.log(__dirname+"/public/images/screen.png");
//    webshot(req.query.h, __dirname+'/public/images/screen.png', function(err) {
//        res.render("share",{params:req.query});
//    });
})

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
